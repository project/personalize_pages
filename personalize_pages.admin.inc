<?php

/**
 * @file
 * Admin page and form callbacks.
 */

/**
 * Callback for the Personalize Pages list page.
 */
function personalize_pages_list() {
  $header = array(
    array('data' => t('Variation set name')),
    array('data' => t('Operations'), 'colspan' => 2),
  );
  $rows = array();

  foreach (personalize_option_set_load_by_type('pages') as $option_set) {
    $tablerow = array(
      array('data' => check_plain($option_set->label)),
      array('data' => l(t('Edit'), 'admin/structure/personalize/variations/personalize-pages/manage/'. $option_set->osid . '/edit')),
      array('data' => l(t('Delete'), 'admin/structure/personalize/variations/personalize-pages/manage/'. $option_set->osid . '/delete')),
    );
    $rows[] = $tablerow;
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No pages available.'), 'colspan' => 3));
  }

  $build = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'personalize_pages'),
  );
  return $build;
}

/**
 * Form callback for the Personalize Page add/edit form.
 */
function personalize_pages_form($form, &$form_state, $formtype, $option_set = NULL) {
  // When the form is in edit mode, the option set is passed as a param.
  $form_state['option_set'] = $option_set;
  if (empty($form_state['option_set'])) {
    $form_state['option_set'] = new StdClass();
    $form_state['option_set']->options = array();
  }

  $form_state['num_pages'] = isset($form_state['num_pages']) ? $form_state['num_pages'] : 2;
  $option_count = count($form_state['option_set']->options);
  if ($option_count > $form_state['num_pages']) {
    $form_state['num_pages'] = $option_count;
  }

  // Ensure that the option_set has the correct number of options.
  for ($i = 0; $i < $form_state['num_pages']; $i++) {
    if (isset($form_state['option_set']->options[$i])) {
      continue;
    }
    $form_state['option_set']->options[$i] = array(
      'option_label' => personalize_generate_option_label($i),
      'weight' => $i,
    );
  }

  $agents = array();
  foreach (personalize_agent_load_multiple() as $agent) {
    $agents[$agent->machine_name] = $agent->label;
  }

  $form['title'] = array(
    '#title' => t('Name'),
    '#description' => t('The name of this variation set.'),
    '#type' => 'textfield',
    '#default_value' => isset($option_set->label) ? $option_set->label : '',
    '#required' => TRUE,
  );

  $form['agent'] = array(
    '#type' => 'select',
    '#title' => t('Campaign'),
    '#description' => t('Select the campaign to use for display of this variation set.'),
    '#options' => $agents,
    '#default_value' => !empty($option_set->agent) ? $option_set->agent : NULL,
    '#required' => TRUE,
  );

  // Add a wrapper for the pages and Add Another Tab button.
  $form['option_sets'] = array(
    '#type' => 'fieldset',
    '#title' => t('Variations'),
    '#description' => t('The first row in the table is where the test will take place. If the first row is %front the personalization will happen on the homepage. If the first row is node/572 the personalization with happen on the page view of node/572.', array('%front' => '<front>')),
    '#attributes' => array('id' => 'personalize-pages-wrapper'),
    '#tree' => TRUE,
    '#theme' => 'personalize_pages_form_variants',
  );

  usort($form_state['option_set']->options, 'drupal_sort_weight');
  foreach ($form_state['option_set']->options as $oid => $page) {

    $form['option_sets'][$oid]['option_label'] = array(
      '#title' => t('Variant label'),
      '#type' => 'textfield',
      '#default_value' => isset($page['option_label']) ? $page['option_label'] : NULL,
    );

    $form['option_sets'][$oid]['path'] = array(
      '#description' => t("Enter the drupal path of a page. Use %front for the front page.", array('%front' => '<front>')),
      '#type' => 'textfield',
      '#default_value' => isset($page['path']) ? $page['path'] : '',
      '#title' => t('Enter a path'),
    );

    // Since we are rendering this form as a table there needs to be an empty 
    // cell for the remove button.
    $form['option_sets'][$oid]['remove'] = array(
      '#markup' => NULL,
    );

    // We only want the remove button to show when there are more than 2 
    // variants because 2 variants is the minimum to create an "Option set".
    if ($form_state['num_pages'] > 2) {
      $form['option_sets'][$oid]['remove'] = array(
        '#type' => 'submit',
        '#value' => t('Remove'),
        '#name' => $oid,
        '#submit' => array('personalize_pages_remove_page_submit'),
        '#limit_validation_errors' => array(),
        '#ajax' => array(
          'callback' => 'personalize_pages_ajax_callback',
          'wrapper' => 'personalize-pages-wrapper',
        ),
      );
    }

    $form['option_sets'][$oid]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Variant weight'),
      '#default_value' => !empty($page['weight']) ? $page['weight'] : $oid,
    );

  }

  $form['pages_more'] = array(
    '#type' => 'submit',
    '#value' => t('Add variation'),
    '#submit' => array('personalize_pages_add_page_submit'),
    '#limit_validation_errors' => array(),
    '#ajax' => array(
      'callback' => 'personalize_pages_ajax_callback',
      'wrapper' => 'personalize-pages-wrapper',
    ),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit_form'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Ajax callback for the add tab and remove tab buttons.
 */
function personalize_pages_ajax_callback($form, $form_state) {
  return $form['option_sets'];
}

/**
 * Submit handler for the "Add Tab" button.
 */
function personalize_pages_add_page_submit($form, &$form_state) {
  // Increment the number of pages to be rendered.
  $form_state['num_pages']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "Remove Tab" button.
 */
function personalize_pages_remove_page_submit($form, &$form_state) {
  if ($form_state['num_pages'] > 2) {
    $delta = $form_state['triggering_element']['#name'];
    array_splice($form_state['input']['option_sets'], $delta, 1);
    array_splice($form_state['option_set']->options, $delta, 1);
    $form_state['num_pages']--;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Form callback for the Personalize Pages delete form.
 */
function personalize_pages_page_delete($form, &$form_state, $personalized_page) {
  $form['osid'] = array('#type' => 'hidden', '#value' => $personalized_page->osid);
  $form['title'] = array('#type' => 'hidden', '#value' => $personalized_page->label);
  return confirm_form($form, t('Are you sure you want to delete the personalized_pages page %title?', array('%title' => $personalized_page->label)), 'admin/structure/personalize/variations/personalize-pages', '', t('Delete'), t('Cancel'));
}


/**
 * Submit handler for Personalize Pages delete form.
 */
function personalize_pages_page_delete_submit($form, &$form_state) {
  personalize_option_set_delete($form_state['values']['osid']);
  drupal_set_message(t('The personalized_pages instance %name has been removed.', array('%name' => $form_state['values']['title'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/personalize/variations/personalize-pages';
}

/**
 * Submit handler for personalize_pages admin page.
 */
function personalize_pages_form_validate($form, &$form_state) {
  foreach ($form_state['values']['option_sets'] as $key => $value) {
    if (!is_array($value) || !isset($value['path'])) {
      continue;
    }

    $paths[$key] = $value['path'];
    $path = $value['path'] == '<front>' ? $value['path'] : drupal_lookup_path('source', $value['path']);
    if (!drupal_valid_path($path)) {
      form_set_error("option_sets][{$key}][path", t('@path does not exist.', array('@path' => $value['path'])));
    }
  }
}

/**
 * Submit handler for personalize_blocks admin page.
 */
function personalize_pages_form_submit($form, &$form_state) {
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op != t('Save')) {
    return;
  }

  $values = $form_state['values'];

  $options = array();
  usort($values['option_sets'], 'drupal_sort_weight');
  foreach ($values['option_sets'] as $option_set) {
    unset($option_set['remove']);
    $options[] = $option_set;
  }

  $option_set = $form_state['option_set'];
  $option_set->label = $values['title'];
  $option_set->plugin = 'pages';
  $option_set->agent = $values['agent'];
  $option_set->options = $options;
  $option_set->executor = 'personalizePages';

  if (isset($values['osid'])) {
    $option_set->osid = $values['osid'];
  }

  personalize_option_set_save($option_set);
  drupal_set_message(t('The option set was saved.'));
  $form_state['redirect'] = 'admin/structure/personalize/variations/personalize-pages';
}
