<?php
/**
 * @file
 * Theme functions for the Personalize Pages module.
 */

/**
 * Callback for theme_personalize_pages_form_variants().
 */
function theme_personalize_pages_form_variants($variables) {
  $variants = $variables['element'];
  $rows = array();
  foreach (element_children($variants) as $key => $delta) {
    $variants[$delta]['weight']['#attributes']['class'] = array('personalization-variant-weight');
    $rows[] = array(
      'data' => array(
        array(
          'data' => $variants[$delta]['option_label'],
        ),
        array(
          'data' => $variants[$delta]['path'],
        ),
        array(
          'data' => $variants[$delta]['remove'],
        ),
        array(
          'data' => $variants[$delta]['weight'],
        ),
      ),
      'class' => array('draggable'),
    );
  }

  $header = array(t('Variant name'), t('Variant'), NULL, t('Weight'));
  $table_id = 'personalization-variants-table';

  $output = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array(
      'id' => $table_id,
      'class' => array('personalization-variants-sortable'),
    ),
  );

  drupal_add_tabledrag($table_id, 'order', 'sibling', 'personalization-variant-weight');
  return drupal_render($output);
}